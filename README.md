# centos-eee image

[Docker](https://www.docker.com) image based on Centos 7.1 to build EEE modules.


Docker pull command:

```
docker pull registry.esss.lu.se/ics-docker/centos-eee:latest
```
