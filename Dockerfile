# Docker image to build EPICS modules in gitlab-ci
FROM europeanspallationsource/centos71-epics:2.1.1

# When using docker in jenkins pipeline, the container is created using:
# "docker run -t -d -u <jenkins_user_uid>:<jenkins_user_gid> ..."
# The jenkins user shall exist in the docker image otherwise git commands fail.
# In gitlab-ci, the user used is the one from the image. We keep the same one as on jenkins.
USER srv_icsjenkins
